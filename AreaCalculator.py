import pygame
import pygame_gui
import math
import sys
from enum import Enum

squareChanged = pygame.event.Event(pygame.USEREVENT)
shapeDeleted = pygame.event.Event(pygame.USEREVENT + 1)

# Класс, описывающий многоугольник
class Shape:
    def __init__(self):
        self.COLOR = (0, 70, 225)
        self.points = []
    
    def square(self):
        if len(self.points) < 3:
            return 0

        res = 0
        for i in range(len(self.points)):
            res += self.getPoint(i)[0] * (self.getPoint(i + 1)[1] - self.getPoint(i - 1)[1])

        return math.fabs(res / 2)

    def getPoint(self, index):
        count = len(self.points)

        if index >= count:
            return self.points[0]
        elif index < 0:
            return self.points[count - 1]

        return self.points[index]

    def draw(self, sc):
        if len(self.points) > 2:
            pygame.draw.polygon(sc, self.COLOR, self.points)
        elif len(self.points) > 1:
            pygame.draw.line(sc, self.COLOR, self.points[0], self.points[1])
    
    def changeLastPointPosition(self, point):
        if(len(self.points) > 0):
            self.points[-1] = point
            pygame.event.post(squareChanged)
    
    def addPoint(self, point):
        self.points.append(point)
        pygame.event.post(squareChanged)
    
    def clear(self):
        self.points.clear()
        pygame.event.post(squareChanged)
        pygame.event.post(shapeDeleted)


if __name__ == "__main__":
    # Инициализация
    W, H = 1280, 720
    BG_COLOR = pygame.Color('#ffffff')
    FPS = 60

    pygame.init()
    pygame.display.set_caption('Area Calculator')

    sc = pygame.display.set_mode((W, H), pygame.RESIZABLE)

    background = pygame.Surface((W, H))
    background.fill(BG_COLOR)

    manager = pygame_gui.UIManager((W, H))


    clock = pygame.time.Clock()
    title = "Area Calculator"

    shape = Shape()

    # Возможные состояния приложения
    class State(Enum):
        NO_SHAPE = 0
        ADDED_POINT = 1,
        MOVED_POINT = 2
        FINAL = 3

    state = State.NO_SHAPE

    # Элементы UI
    noShapeText = 'Фигура отсутствует'

    squareLabelRect = pygame.Rect((0, 0), (250, 30))
    squareLabel = pygame_gui.elements.UILabel(squareLabelRect, '', manager)
    squareLabel.set_text(noShapeText)

    # Главный цикл приложения
    while True:
        sc.blit(background, (0, 0))

        # Обработка событий клавиатуры и мыши
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                if state == State.FINAL:
                    shape.clear()
                    state = State.NO_SHAPE
                elif state == State.NO_SHAPE or state == State.MOVED_POINT:
                    shape.addPoint(event.pos)
                    state = State.ADDED_POINT
            elif event.type == pygame.MOUSEMOTION:
                if state == State.ADDED_POINT:
                    shape.addPoint(event.pos)
                    state = State.MOVED_POINT
                elif state == State.MOVED_POINT:
                    shape.changeLastPointPosition(event.pos)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE or\
                 event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
                    if state != State.NO_SHAPE:
                        state = State.FINAL
            elif event == squareChanged:
                squareLabel.set_text('Площадь фигуры: ' + str(math.floor(shape.square())) + ' px')
            elif event == shapeDeleted:
                squareLabel.set_text(noShapeText)
            
            manager.process_events(event)
        
        # Отрисовка
        shape.draw(sc)
        manager.draw_ui(sc)
        pygame.display.update()

        clock.tick(FPS)