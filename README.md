# AreaCalculator

AreaCalculator - программа, измеряющая площадь многоугольника, нарисованного пользователем на экране.

## Инструкция по испозьзованию

+ Склонировать репозиторий командой `git clone https://gitlab.com/sovaz1997/areacalculator.git`, либо просто скачать
+ Перейти в скачанную директорию в терминале
+ Скачать интерпретатор Python 3 и установить: https://www.python.org/downloads/release/python-376/
+ Установить зависимости командой в терминале: `pip3 install -r requirements.txt`
+ Запустить программу командой: `python main.py`
